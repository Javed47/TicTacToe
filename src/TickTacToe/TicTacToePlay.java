package TickTacToe;

public class TicTacToePlay {
    public static void main(String[] args) {
        // Initialize and instantiate the game
        TicTacToe play = new TicTacToe();

        // Play the game once
        do {
            play.playerMove(play.getCurrentPlayer());
            play.updateGame(play.getCurrentPlayer(), play.getCurrntRow(), play.getCurrentCol());
            play.printBoard();
            // Printing message of game staus
            if (play.getCurrentState() == 2) {
                System.out.println("'X' won! Bye!");
            } else if (play.getCurrentState() == 3) {
                System.out.println("'O' won! Bye!");
            } else if (play.getCurrentState() == 1) {
                System.out.println("It's a Draw! Bye!");
            }
            // Switch player
            play.changePlayer();
        } while (play.getCurrentState()== 0); // repeat if not game-over
    }
}
