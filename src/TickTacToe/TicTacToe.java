package TickTacToe;
import java.util.Scanner;

public class TicTacToe {
//    Numbering the empty cross and nought to work with these in the main function
    private final int empty = 0;
    private final int cross = 1;
    private final int nought = 2;
//  Numbering the game status
    private final int playing = 0;
    private final int draw = 1;
    private final int crossWon = 2;
    private final int noughtWon = 3;
//  Initialization of the board with fixed rows and columns
    private final int rows = 3, cols = 3;
    private int[][] board = new int[rows][cols];
//  Securing the Current Player current Row and Column
    private int currentPlayer;
    private int currentRow, currentCol;
    private int currentState;
//  Instantiating the Input Scanner
    private Scanner in = new Scanner(System.in);

    //  Constructor which will also initialize the game
    public TicTacToe(){
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                board[row][col] = empty;  // all cells empty
            }
        }
        currentState = playing; // ready to play
        currentPlayer = cross;
    }
//  To change the player in main function
    public void changePlayer(){
        currentPlayer = (currentPlayer == cross) ? nought : cross;
    }

    public int getCurrentPlayer(){
        return currentPlayer;
    }

    public int getCurrntRow(){
        return currentRow;
    }

    public int getCurrentCol(){
        return currentCol;
    }

    public int getCurrentState(){
        return currentState;
    }
//  To determine the players Move
    public void playerMove(int theSeed) {
        boolean validInput = false;  // for input validation
        do {
            if (theSeed == cross) {
                System.out.print("Player 'X', enter your move (row[1-3] column[1-3]): ");
            } else {
                System.out.print("Player 'O', enter your move (row[1-3] column[1-3]): ");
            }
            int row = in.nextInt() - 1;  // Taking the input into row and column
            int col = in.nextInt() - 1;
            if (row >= 0 && row < rows && col >= 0 && col < cols && board[row][col] == empty) {
                currentRow = row;
                currentCol = col;
                board[currentRow][currentCol] = theSeed;  // update game-board content
                validInput = true;  // input okay, exit loop
                
            } else {
                System.out.println("This move at (" + (row + 1) + "," + (col + 1)
                        + ") is not valid. Try again...");
            }
        } while (!validInput);  // repeat until input is valid
    }

    public void updateGame(int theSeed, int currentRow, int currentCol) {
        if (hasWon(theSeed, currentRow, currentCol)) {  // check if winning move
            currentState = (theSeed == cross) ? crossWon : noughtWon;
        } else if (isDraw()) {  // check for draw
            currentState = draw;
        }
        // Otherwise, no change to currentState (still PLAYING).
    }

    public  boolean isDraw() {
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                if (board[row][col] == empty) {
                    return false;  // an empty cell found, not draw, exit
                }
            }
        }
        return true;  // no empty cell, it's a draw
    }

    public boolean hasWon(int theSeed, int currentRow, int currentCol) {
        return (board[currentRow][0] == theSeed         // 3-in-the-row
                && board[currentRow][1] == theSeed
                && board[currentRow][2] == theSeed
                || board[0][currentCol] == theSeed      // 3-in-the-column
                && board[1][currentCol] == theSeed
                && board[2][currentCol] == theSeed
                || currentRow == currentCol            // 3-in-the-diagonal
                && board[0][0] == theSeed
                && board[1][1] == theSeed
                && board[2][2] == theSeed
                || currentRow + currentCol == 2  // 3-in-the-opposite-diagonal
                && board[0][2] == theSeed
                && board[1][1] == theSeed
                && board[2][0] == theSeed);
    }

    public void printBoard() {
        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                printCell(board[row][col]); // print each of the cells
                if (col != cols - 1) {
                    System.out.print("|");   // print vertical partition
                }
            }
            System.out.println();
            if (row != rows - 1) {
                System.out.println("-----------"); // print horizontal partition
            }
        }
        System.out.println();
    }

    public void printCell(int content) {
        switch (content) {
            case empty:  System.out.print("   "); break;
            case nought: System.out.print(" O "); break;
            case cross:  System.out.print(" X "); break;
        }
    }
}
